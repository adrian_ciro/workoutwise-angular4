var gulp = require('gulp'),
    del = require('del'),
    merge = require('merge-stream'),

    tsc = require('gulp-typescript'),
    tsProject = tsc.createProject('tsconfig.json'),
    SystemBuilder = require('systemjs-builder'),
    jsMinify = require('gulp-uglify'),

    mocha = require('gulp-mocha'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin'),

    less = require('gulp-less'),
    cssPrefixer = require('gulp-autoprefixer'),
    cssMinify = require('gulp-cssnano');

gulp.task('clean', function() {
    del('build');
    return del('dist');
});

gulp.task('shims', function() {
    return gulp.src([
            'node_modules/core-js/client/shim.js',
            'node_modules/zone.js/dist/zone.js',
            'node_modules/reflect-metadata/Reflect.js'
        ])
        .pipe(concat('shims.js'))
        .pipe(gulp.dest('dist/js/'));
});

gulp.task('system-build', [ 'tsc' ], function() {
    var builder = new SystemBuilder();

    return builder.loadConfig('system.config.js')
        .then(function(){ builder.buildStatic('app', 'dist/js/bundle.js', {
            production: false,
            rollup: false
            });
        });
});

gulp.task('tsc', function() {
    del('build');

    return gulp.src('src/app/**/*.ts')
        .pipe(tsProject())
        .pipe(gulp.dest('build/'));
});

gulp.task('html', function() {
    return gulp.src('src/**/**.html')
        .pipe(gulp.dest('dist/'));
});

gulp.task('images', function() {
    return gulp.src('src/assets/images/**/*.*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images/'));
});

gulp.task('less', function() {
    return gulp.src('src/assets/less/_main.less')
        .pipe(less())
        .pipe(concat('styles.css'))
        .pipe(cssPrefixer())
        .pipe(gulp.dest('dist/css/'));
});

gulp.task('copyfiles', function() {
    gulp.src(['src/.htaccess', 'src/favicon.ico', 'src/robots.txt', 'src/404.html'])
        .pipe(gulp.dest('./dist/'));
});



//TO RUN INDEPENDENTLY

gulp.task('test', [ 'test-run' ], function() {
    return del('build');
});

gulp.task('minify', function() {
    var js = gulp.src('dist/js/bundle.js')
        .pipe(jsMinify())
        .pipe(gulp.dest('dist/js/'));

    var js2 = gulp.src('dist/js/shims.js')
        .pipe(jsMinify())
        .pipe(gulp.dest('dist/js/'));

    var css = gulp.src('dist/css/styles.css')
        .pipe(cssMinify())
        .pipe(gulp.dest('dist/css/'));

    return merge(js, js2, css);
});

gulp.task('watch', function() {
    var watchTs = gulp.watch('src/app/**/**.ts', [ 'system-build' ]),
        watchLess = gulp.watch('src/assets/**/*.less', [ 'less' ]),
        watchHtml = gulp.watch('src/**/*.html', [ 'html' ]),
        watchImages = gulp.watch('src/assets/images/**/*.*', [ 'images' ]),

        onChanged = function(event) {
            console.log('File ' + event.path + ' was ' + event.type + '. Running tasks...');
        };

    watchTs.on('change', onChanged);
    watchLess.on('change', onChanged);
    watchHtml.on('change', onChanged);
    watchImages.on('change', onChanged);
});

gulp.task('watchtests', function() {
    var watchTs = gulp.watch('src/app/**/**.ts', [ 'test-run' ]),
        watchTests = gulp.watch('test/**/*.spec.js', [ 'test-run' ]),

    onChanged = function(event) {
        console.log('File ' + event.path + ' was ' + event.type + '. Running tasks...');
    };

    watchTs.on('change', onChanged);
    watchTests.on('change', onChanged);
});

gulp.task('default', [
    'shims',
    'system-build',
    'html',
    'images',
    'less',
    'copyfiles'
]);

