import { Routes, RouterModule } from '@angular/router';

import {LoginComponent} from './login/login.component';
import {SignupComponent} from "./signup/signup.component";
import {HomeComponent} from "./home/home.component";
import {AuthGuard} from "./service/authGuard.service";
import {NotFoundComponent} from "./not-found/not-found.component";
import {WorkoutComponent} from "./workout/workout.component";
import {ActivityComponent} from "./activity/activity.component";
import {BodyComponent} from "./body/body.component";
import {SettingsComponent} from "./settings/settings.component";
import {HelpComponent} from "./help/help.component";
import {HistoryComponent} from "./history/history.component";

const ROUTES: Routes = [
    { path: '',       component: LoginComponent },
    { path: 'login',  component: LoginComponent },
    { path: 'signup', component: SignupComponent },
    { path: 'home',   component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'workouts',   component: WorkoutComponent, canActivate: [AuthGuard] },
    { path: 'activities',   component: ActivityComponent, canActivate: [AuthGuard] },
    { path: 'body',   component: BodyComponent, canActivate: [AuthGuard] },
    { path: 'history',   component: HistoryComponent, canActivate: [AuthGuard] },
    { path: 'settings',   component: SettingsComponent, canActivate: [AuthGuard] },
    { path: 'help',   component: HelpComponent, canActivate: [AuthGuard] },
    { path: '**',     component: NotFoundComponent },
];

export const ROUTING = RouterModule.forRoot(ROUTES);

