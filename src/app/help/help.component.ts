import { Component } from '@angular/core';
import {Router} from "@angular/router";

@Component({
    selector: 'help',
    templateUrl: 'app/help/help.component.html'
})
export class HelpComponent {
    constructor(public router: Router) {}
}