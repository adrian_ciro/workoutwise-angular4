import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Http, RequestOptions } from '@angular/http';
import { contentHeaders } from '../common/headers';
import {AuthGuard} from "../service/authGuard.service";
import {Facebook} from "../service/facebook.service";
import {Api} from "../common/api";

declare var FB: any;

@Component({
    selector: 'login',
    templateUrl: 'app/login/login.component.html'
})
export class LoginComponent{

    private msgPrefix = 'signin.';
    private error = '';

    constructor(public router: Router, public http: Http, public authGuard: AuthGuard, public fb: Facebook) {
        if(authGuard.isAuthenticated() || fb.isLoggedWithFb()){
            this.router.navigate(['home']);
        }
    }

    login(event:any, username:string, password:string, rememberMe: string) {
        event.preventDefault();
        this.error = '';

        if(username == '' || password == ''){
            this.error = this.msgPrefix+'error.fieldisempty';
            return;
        }

        let body = JSON.stringify({ username, password, rememberMe });
        let options:RequestOptions = new RequestOptions({headers: contentHeaders});

        this.http.post(Api.API_ENDPOINT_DEV+'authenticate', body, options)
            .subscribe(
                response => {
                    localStorage.setItem('id_token', response.json().id_token);
                    localStorage.setItem('current_user', username);
                    this.router.navigate(['home']);
                },
                error => {
                    if(error.status == 400){
                        this.error = this.msgPrefix+ error.json().message;
                    }
                    else{
                        this.error = this.msgPrefix+error.json().accessdenied;
                    }
                    // console.log(error.text());
                }
            );
    }

}