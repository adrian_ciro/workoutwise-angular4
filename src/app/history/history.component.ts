import { Component } from '@angular/core';
import {Router} from "@angular/router";

@Component({
    selector: 'history',
    templateUrl: 'app/history/history.component.html'
})
export class HistoryComponent {
    constructor(public router: Router) {}
}