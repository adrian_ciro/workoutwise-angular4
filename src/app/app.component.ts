import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {Facebook} from "./service/facebook.service";
import {TranslateService} from "./service/translate/translate.service";

@Component({
    selector: 'app-component',
    templateUrl: 'app/app.component.html'
})
export class AppComponent {

    public translatedText: string;
    public supportedLanguages: any[];

    constructor(public router: Router, public fb: Facebook, private translate: TranslateService) {
        fb.loadAndInitFBSDK();

        this.supportedLanguages = [
            { display: 'English', value: 'en' },
            { display: 'Español', value: 'es' }
        ];

        this.selectLang('en');
    }

    isCurrentLang(lang: string) {
        return lang === this.translate.currentLang;
    }

    selectLang(lang: string) {
        // set default;
        this.translate.use(lang);
    }

}

