import { Headers } from '@angular/http';

export const contentHeaders = new Headers();
export const authorizationHeader = 'Authorization';
export const authorizationValue = 'Bearer ';

contentHeaders.append('Accept', 'application/json');
contentHeaders.append('Content-Type', 'application/json');

export function addAuthorization(jwt:string){
    if(!contentHeaders.has(authorizationHeader)){
        contentHeaders.append(authorizationHeader, authorizationValue + jwt);
    }
}