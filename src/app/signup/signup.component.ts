import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {Http} from '@angular/http';
import {contentHeaders} from '../common/headers';
import {AuthGuard} from "../service/authGuard.service";
import {Facebook} from "../service/facebook.service";
import {TranslateService} from "../service/translate/translate.service";
import {Api} from "../common/api";

@Component({
    selector: 'signup',
    templateUrl: 'app/signup/signup.component.html'
})
export class SignupComponent {

    private msgPrefix = 'signup.';
    private error = '';

    constructor(public router: Router, public http: Http, public authGuard: AuthGuard,
                public fb: Facebook, private translate: TranslateService) {
        if (authGuard.isAuthenticated() || fb.isLoggedWithFb()) {
            this.router.navigate(['home']);
        }
    }

    signup(event: any, login: string, password: string, email: string, password2: string) {
        event.preventDefault();
        this.error = '';

        if (login == '' || password == '' || email == '' || password2 == '') {
            this.error = this.msgPrefix + 'validation.emptyfields';
            return;
        }

        if (password != password2) {
            this.error = this.msgPrefix +'validation.samepassword';
            return;
        }

        let body = JSON.stringify({login, password, email, langKey: this.translate.currentLang});
        this.http.post(Api.API_ENDPOINT_DEV+'/register', body, {headers: contentHeaders})
            .subscribe(
                response => {
                    localStorage.setItem('id_token', response.json().id_token);
                    this.router.navigate(['home']);
                },
                error => {
                    if (error.json().message) {
                        this.error = this.msgPrefix + error.json().message;
                    }
                    this.error = this.msgPrefix +'error.fail';
                    console.log(error.text());
                }
            );
    }

}
