import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from "./home/home.component";
import {SignupComponent} from "./signup/signup.component";
import {NotFoundComponent} from "./not-found/not-found.component";
import {NavComponent} from "./nav/nav.component";
import {WorkoutComponent} from "./workout/workout.component";
import {ActivityComponent} from "./activity/activity.component";
import {BodyComponent} from "./body/body.component";
import {HelpComponent} from "./help/help.component";
import {SettingsComponent} from "./settings/settings.component";
import {HistoryComponent} from "./history/history.component";

import {AuthGuard} from "./service/authGuard.service";
import {Facebook} from "./service/facebook.service";

import {ROUTING} from './app.routes';
import {AUTH_PROVIDERS} from "angular2-jwt";
import {TRANSLATION_PROVIDERS, TranslatePipe, TranslateService}   from './service/translate/translate';

@NgModule({
    imports: [ HttpModule, BrowserModule, FormsModule, ROUTING ],
    declarations: [
        AppComponent,
        HomeComponent,
        SignupComponent,
        LoginComponent,
        NotFoundComponent,
        TranslatePipe,
        NavComponent,
        WorkoutComponent,
        ActivityComponent,
        BodyComponent,
        HelpComponent,
        SettingsComponent,
        HistoryComponent
    ],
    providers: [
        Facebook,
        AuthGuard,
        ...AUTH_PROVIDERS,
        TRANSLATION_PROVIDERS, TranslateService
    ],
    bootstrap: [ AppComponent ]
})
export class AppModule { }

