import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {Http, RequestOptions} from "@angular/http";
import {AuthGuard} from "../service/authGuard.service";
import {workout} from "./workout.model";
import {addAuthorization, contentHeaders} from "../common/headers";
import {Api} from "../common/api";

declare var $: any;

@Component({
    selector: 'workout',
    templateUrl: 'app/workout/workout.component.html'
})
export class WorkoutComponent {

    private model:workout;
    private error = '';
    private msgPrefix = 'workout.';
    private isupdating = false;
    private hideModal = false;
    private success = false;
    private alertMsg = '';
    private modalTitle = 'create';
    private currentId: number;

    private workouts:Array<workout> = [];
    private difficulties: Array<String> = [];
    private activities: Array<String> = [];

    constructor(public router: Router, public http: Http, public authGuard: AuthGuard) {
        this.initialize();
    }

    initialize() {
        this.model = new workout();
        let id_token = this.authGuard.getToken();
        addAuthorization(id_token);
        let body = JSON.stringify({ id_token });
        let options:RequestOptions = new RequestOptions({headers: contentHeaders});

        this.http.post(Api.API_ENDPOINT_DEV+'workouts', body, options)
            .subscribe(
                response => {
                    this.difficulties =  response.json().difficulties;
                    this.activities =  response.json().activities;
                    this.workouts = response.json().workouts;
                },
                error => {
                    this.difficulties =  [];
                    this.activities =  [];
                    this.error = this.msgPrefix+ error.json().message;
                    console.log(error.text());
                }
            );
    }

    createWorkout(){
        let id_token = this.authGuard.getToken();
        addAuthorization(id_token);
        let name = this.model.name;
        let description = this.model.description;
        let difficulty = this.model.difficulty;
        let activities = this.model.activities;
        let options:RequestOptions = new RequestOptions({headers: contentHeaders});

        if(!this.isupdating){
            let body = JSON.stringify({ id_token, name, description, difficulty, activities });
            this.http.post(Api.API_ENDPOINT_DEV+'workout', body, options)
                .subscribe(
                    response => {
                        this.handleResponse('creationsuccess');
                    },
                    error => {
                        this.error = this.msgPrefix+ error.json().message;
                        console.log(error.text());
                    }
                );
        }
        else{
            let id = this.model.id;
            let body = JSON.stringify({ id_token, id, name, description, difficulty, activities });
            this.http.put(Api.API_ENDPOINT_DEV+'workout', body, options)
                .subscribe(
                    response => {
                        this.handleResponse('updatesuccess');
                    },
                    error => {
                        this.error = this.msgPrefix+ error.json().message;
                        console.log(error.text());
                    }
                );
        }

    }

    deleteWorkout(){
        let id_token = this.authGuard.getToken();
        addAuthorization(id_token);
        let options:RequestOptions = new RequestOptions({headers: contentHeaders});

        this.http.delete(Api.API_ENDPOINT_DEV+'workout/'+this.currentId, options)
            .subscribe(
                response => {
                    // setTimeout(()=> $('#deleteWorkoutModal').modal('hide'), 2000);
                    this.initialize();
                },
                error => {
                    this.error = this.msgPrefix+ error.json().message;
                    console.log(error.text());
                }
            );
    }

    getWorkout(id:number){
        this.clean();
        this.isupdating=true;
        this.modalTitle = 'update';
        let workout:workout = this.workouts.find(value => value.id==id);
        this.model.id = workout.id;
        this.model.name = workout.name;
        this.model.description = workout.description;
        this.model.difficulty = workout.difficulty;
        this.model.activities = workout.activities;
    }

    setWorkoutID(id:number){
       this.currentId = id;
    }

    clean(){
        this.model = new workout();
        this.hideModal=false;
        this.success=false;
        this.isupdating=false;
        this.error = '';
        this.alertMsg = '';
        this.modalTitle = 'create';
    }

    handleResponse(alertMsg:string){
        this.hideModal=true;
        this.success=true;
        this.alertMsg = alertMsg;
        setTimeout(()=> $('#createActivityModal').modal('hide'), 2000);
        setTimeout(()=> this.clean(), 2100);
        this.initialize();
    }
}