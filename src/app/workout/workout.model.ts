/**
 * Created by adrian on 6/12/17.
 */

export class workout {
    constructor()
    constructor(
        public id?: number,
        public wkImage?: any,
        public name?: string,
        public description?: string,
        public duration?: number,
        public durationUom?: string,
        public difficulty?: string,
        public activities?: string[],
    ) {  }
}