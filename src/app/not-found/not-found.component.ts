import { Component } from '@angular/core';
import {Router} from "@angular/router";

@Component({
    selector: 'not-found',
    templateUrl: 'app/not-found/not-found.component.html'
})
export class NotFoundComponent {
    constructor(public router: Router) {}
}