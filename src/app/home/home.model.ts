/**
 * Created by adrian on 5/31/17.
 */
export class Overview {
    phrase: string;
    totalWorkouts: number;
    totalActivities: number;
    weight: number;
    height: number;
    historyActivities: Array<HistoryActivity>;
}

export interface HistoryActivity {
    name: string;
    duration: number;
    sets: number;
    repetitions: number;
    timeXRepetition: number;
    weightToLift: number;
    calories: number;
    distance: number;
    startDate: string;
    finishDate: string;
}