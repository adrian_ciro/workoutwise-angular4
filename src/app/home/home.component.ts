import {Component} from '@angular/core';
import {Http, RequestOptions} from '@angular/http';
import {Router} from '@angular/router';
import {Api} from "../common/api";
import {addAuthorization, contentHeaders} from "../common/headers";
import {Overview} from "./home.model";
import {AuthGuard} from "../service/authGuard.service";

@Component({
  selector: 'home',
  templateUrl: 'app/home/home.component.html'
})
export class HomeComponent {

  private msgPrefix = 'overview.';
  private error = '';
  private model: Overview;

  constructor(public router: Router, public http: Http, public authGuard: AuthGuard) {
    this.initialize();
  }

  initialize() {
    this.model = new Overview();
    let body = {};
    addAuthorization(this.authGuard.getToken());
    let options:RequestOptions = new RequestOptions({headers: contentHeaders});

    this.http.post(Api.API_ENDPOINT_DEV+'overview', body, options)
        .subscribe(
            response => {
              this.model = {
                phrase: response.json().phrase,
                totalWorkouts: response.json().totalWorkouts,
                totalActivities: response.json().totalActivities,
                weight: response.json().weight,
                height: response.json().height,
                historyActivities: response.json().historyActivities
              };

            },
            error => {
                this.error = this.msgPrefix + error.json().message;
            }
        );
  }

}
