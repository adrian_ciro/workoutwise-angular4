export class activity {
    constructor()
    constructor(
        public id?: number,
        public wkImage?: any,
        public name?: string,
        public description?: string,
        public duration?: number,
        public durationUom?: string,
        public muscle?: string,
        public isWithMachine?: boolean,
        public sets?: number,
        public repetitions?: number,
        public timeXRepetition?: number,
        public weightToLift?: number,
        public calories?: number,
        public distance?: number,
        public workouts?: string[],
    ) {  }
}