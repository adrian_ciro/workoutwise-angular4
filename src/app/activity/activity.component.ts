import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {Http, RequestOptions} from "@angular/http";
import {AuthGuard} from "../service/authGuard.service";
import {activity} from "./activity.model";
import {addAuthorization, contentHeaders} from "../common/headers";
import {Api} from "../common/api";
import {workout} from "../workout/workout.model";

declare var $: any;

@Component({
    selector: 'activity',
    templateUrl: 'app/activity/activity.component.html'
})
export class ActivityComponent {

    private model:activity;
    private error = '';
    private msgPrefix = 'activity.';
    private isupdating = false;
    private hideModal = false;
    private success = false;
    private alertMsg = '';
    private modalTitle = 'create';
    private currentId: number;

    private activities:Array<activity> = [];
    private muscleList: Array<String> = [];

    constructor(public router: Router, public http: Http, public authGuard: AuthGuard) {
        this.initialize();
    }

    initialize() {
        this.model = new activity();
        let id_token = this.authGuard.getToken();
        addAuthorization(id_token);
        let body = JSON.stringify({ id_token });
        let options:RequestOptions = new RequestOptions({headers: contentHeaders});

        this.http.post(Api.API_ENDPOINT_DEV+'activities', body, options)
            .subscribe(
                response => {
                    this.muscleList =  response.json().muscleList;
                    this.activities =  response.json().activities;
                },
                error => {
                    this.muscleList =  [];
                    this.activities =  [];
                    this.error = this.msgPrefix+ error.json().message;
                    console.log(error.text());
                }
            );
    }

    createActivity(){
        let id_token = this.authGuard.getToken();
        addAuthorization(id_token);
        let name = this.model.name;
        let description = this.model.description;

        let options:RequestOptions = new RequestOptions({headers: contentHeaders});

        if(!this.isupdating){
            let body = JSON.stringify({ id_token, name, description });
            this.http.post(Api.API_ENDPOINT_DEV+'activity', body, options)
                .subscribe(
                    response => {
                        this.handleResponse('creationsuccess');
                    },
                    error => {
                        this.error = this.msgPrefix+ error.json().message;
                        console.log(error.text());
                    }
                );
        }
        else{
            let id = this.model.id;
            let body = JSON.stringify({ id_token, id, name, description });
            this.http.put(Api.API_ENDPOINT_DEV+'activity', body, options)
                .subscribe(
                    response => {
                        this.handleResponse('updatesuccess');
                    },
                    error => {
                        this.error = this.msgPrefix+ error.json().message;
                        console.log(error.text());
                    }
                );
        }

    }

    deleteActivity(){
        let id_token = this.authGuard.getToken();
        addAuthorization(id_token);
        let options:RequestOptions = new RequestOptions({headers: contentHeaders});

        this.http.delete(Api.API_ENDPOINT_DEV+'activities/'+this.currentId, options)
            .subscribe(
                response => {
                    // setTimeout(()=> $('#deleteActivityModal').modal('hide'), 2000);
                    this.initialize();
                },
                error => {
                    this.error = this.msgPrefix+ error.json().message;
                    console.log(error.text());
                }
            );
    }

    getActivity(id:number){
        this.clean();
        this.isupdating=true;
        this.modalTitle = 'update';
        // let workout:workout = this.activities.find(value => value.id==id);
        // this.model.id = workout.id;
        // this.model.name = workout.name;
        // this.model.description = workout.description;
        // this.model.difficulty = workout.difficulty;
    }

    setActivityID(id:number){
        this.currentId = id;
    }

    clean(){
        this.model = new workout();
        this.hideModal=false;
        this.success=false;
        this.isupdating=false;
        this.error = '';
        this.alertMsg = '';
        this.modalTitle = 'create';
    }

    handleResponse(alertMsg:string){
        this.hideModal=true;
        this.success=true;
        this.alertMsg = alertMsg;
        setTimeout(()=> $('#createActivityModal').modal('hide'), 2000);
        setTimeout(()=> this.clean(), 2100);
        this.initialize();
    }
}