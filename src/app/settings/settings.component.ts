import { Component } from '@angular/core';
import {Router} from "@angular/router";

@Component({
    selector: 'settings',
    templateUrl: 'app/settings/settings.component.html'
})
export class SettingsComponent {
    constructor(public router: Router) {}
}