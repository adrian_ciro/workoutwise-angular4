import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {Http, RequestOptions} from "@angular/http";
import {addAuthorization, contentHeaders} from "../common/headers";
import {Api} from "../common/api";
import {AuthGuard} from "../service/authGuard.service";
import {body} from "./body.model";

declare var $: any;

@Component({
    selector: 'body',
    templateUrl: 'app/body/body.component.html'
})
export class BodyComponent {

    private model:body;
    private error = '';
    private msgPrefix = 'body.';
    private hideModal = false;
    private success = false;

    constructor(public router: Router, public http: Http, public authGuard: AuthGuard) {
        this.initialize();
    }

    initialize() {
        this.model = new body();
        let id_token = this.authGuard.getToken();
        addAuthorization(id_token);
        let bodyjson = JSON.stringify({ id_token });
        let options:RequestOptions = new RequestOptions({headers: contentHeaders});

        this.http.post(Api.API_ENDPOINT_DEV+'mybody', bodyjson, options)
            .subscribe(
                response => {
                    this.model.id = response.json().id;
                    this.model.weight = response.json().weight;
                    this.model.height = response.json().height;
                    this.model.fat = response.json().fat;
                    this.model.muscle = response.json().muscle;
                },
                error => {
                    this.error = this.msgPrefix+ error.json().message;
                    console.log(error.text());
                }
            );
    }

    update(){
        let id_token = this.authGuard.getToken();
        addAuthorization(id_token);

        let body = JSON.stringify({ id_token });
        let options:RequestOptions = new RequestOptions({headers: contentHeaders});

        this.http.put(Api.API_ENDPOINT_DEV+'mybody', this.model, options)
            .subscribe(
                response => {
                    this.hideModal=true;
                    this.success=true;
                    setTimeout(()=> $('#updateBodyModal').modal('hide'), 2000);
                },
                error => {
                    this.error = this.msgPrefix+ error.json().message;
                    console.log(error.text());
                }
            );
    }

    clean(){
        this.hideModal=false;
        this.success=false;
        this.error = '';
    }
}