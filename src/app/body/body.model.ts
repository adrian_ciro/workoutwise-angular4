export class body {
    constructor()
    constructor(
        public id?: number,
        public weight?: number,
        public height?: number,
        public fat?: number,
        public muscle?: number
    ) {  }
}