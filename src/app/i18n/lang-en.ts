// lang-en.ts

export const LANG_EN_NAME = 'en';

export const LANG_EN_TRANS = {

    //General
    'general.yes': 'Yes',
    'general.no': 'No',
    'general.form.button.create': 'Create',
    'general.form.button.update': 'Update',
    'general.form.button.cancel': 'Cancel',

    //Menu
    'menuitem.overview': 'Overview',
    'menuitem.workouts': 'Workouts',
    'menuitem.activities': 'Activities',
    'menuitem.history': 'History',
    'menuitem.mybody': 'My body',
    'menuitem.settings': 'Settings',
    'menuitem.help': 'Help',
    'menuitem.logout': 'Log out',

    //Sign up
    'signup.form.title': 'Sign up',
    'signup.form.username': 'Username',
    'signup.form.email': 'Email address',
    'signup.form.password': 'Password',
    'signup.form.password2': 'Confirm Password',
    'signup.form.button.submit': 'Sign up',
    'signup.form.login': 'Or sign in',
    'signup.validation.emptyfields': 'Please fill up all the fields',
    'signup.validation.samepassword': 'Please put same password in both fields.',
    'signup.success': 'Registration saved!',
    'signup.error.fail': 'Registration failed! Please try again later.',
    'signup.error.loginexists': 'Login name already registered! Please choose another one.',
    'signup.error.emailexists': 'Email is already in use! Please choose another one.',
    'signup.error.validation': 'One or some fields are incorrect. Check the length of the inputs or whether the email is valid.',

    //Sign in
    'signin.form.title': 'Sign in',
    'signin.form.username': 'Username',
    'signin.form.password': 'Password',
    'signin.form.rememberme': 'Remember me',
    'signin.form.signup': 'Or Sign up',
    'signin.form.forgotpassword': 'Forgot password?',
    'signin.form.button.submit': 'Sign in',
    'signin.error.fieldisempty': 'Username or password is empty',
    'signin.error.accessdenied': 'Failed to sign in! Please try again',

    //Activity
    'activity.form.button.create': 'Create activity',
    'activity.form.button.update': 'Update activity',
    'activity.modal.name': 'Name',
    'activity.modal.description': 'Description',
    'activity.modal.muscle': 'Associated muscle',
    'activity.modal.duration': 'Duration',
    'activity.modal.muscle.arms': 'Arms',
    'activity.modal.muscle.legs': 'Legs',
    'activity.modal.muscle.chest': 'Chest',
    'activity.modal.muscle.shoulders': 'Shoulders',
    'activity.modal.muscle.abs': 'Abs',
    'activity.modal.muscle.back': 'Back',
    'activity.modal.delete.confirmation': 'Are you sure you want to delete the activity?',
    'activity.msg.creationsuccess': 'Success! The activity was created',
    'activity.msg.updatesuccess': 'Success! The activity was updated',
    'activity.msg.emptyactivity.title': 'First time here huh',
    'activity.msg.emptyactivity.content': 'Please create your first activity',

    //Workout
    'workout.form.button.create': 'Create workout',
    'workout.form.button.update': 'Update workout',
    'workout.modal.name': 'Name',
    'workout.modal.description': 'Description',
    'workout.modal.difficulty': 'Difficulty',
    'workout.modal.duration': 'Duration',
    'workout.modal.activities': 'Activities',
    'workout.modal.difficult.easy': 'Easy',
    'workout.modal.difficult.normal': 'Normal',
    'workout.modal.difficult.hard': 'Hard',
    'workout.modal.delete.confirmation': 'Are you sure you want to delete the workout?',
    'workout.error.internalservererror': 'Error! A problem has been occurred while submitting your data',
    'workout.error.workoutexists': 'Error! There is a workout with the same name. Please choose another name',
    'workout.msg.creationsuccess': 'Success! The workout was created',
    'workout.msg.updatesuccess': 'Success! The workout was updated',
    'workout.msg.createactivity.title': 'Ups... Activities first',
    'workout.msg.createactivity.content': 'In order to create workouts, first we need you to create activities.',

    //My body
    'body.form.button.update': 'Update my body',
    'body.modal.weight': 'Weight',
    'body.modal.height': 'Height',
    'body.modal.fat': 'Fat percentage',
    'body.modal.muscle': 'Muscle percentage',

    //Errors
    'error.internalservererror': 'Error! A problem has been occurred while submitting your data',

};