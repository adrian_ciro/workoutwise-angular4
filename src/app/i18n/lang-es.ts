// lang-es.ts

export const LANG_ES_NAME = 'es';

export const LANG_ES_TRANS = {

    //General
    'general.yes': 'Si',
    'general.no': 'No',
    'general.form.button.create': 'Crear',
    'general.form.button.update': 'Actualizar',
    'general.form.button.cancel': 'Cancelar',

    //Menu
    'menuitem.overview': 'Información general',
    'menuitem.workouts': 'Entrenamientos',
    'menuitem.activities': 'Actividades',
    'menuitem.history': 'Historial',
    'menuitem.mybody': 'Mi cuerpo',
    'menuitem.settings': 'Configuración',
    'menuitem.help': 'Ayuda',
    'menuitem.logout': 'Cerrar sesión',

    //Registrarse
    'signup.form.title': 'Registrarse',
    'signup.form.username': 'Usuario',
    'signup.form.email': 'Email',
    'signup.form.password': 'Contraseña',
    'signup.form.password2': 'Confirmar contraseña',
    'signup.form.button.submit': 'Crear cuenta',
    'signup.form.login': 'O iniciar sesión',
    'signup.validation.emptyfields': 'Por favor llenar todos los campos',
    'signup.validation.samepassword': 'Por favor poner la misma contraseña',
    'signup.success': 'Cuenta creada exitosamente!',
    'signup.error.fail': 'Error en la creacion de usuario! Intente nuevamente.',
    'signup.error.loginexists': 'El ususario ya existe en nuestro sistema! Por favor elija otro.',
    'signup.error.emailexists': 'El email ya existe en nuestro sistema! Por favor elija otro.',
    'signup.error.validation': 'Alguno de los campos es invalido. Revise la longitud de los caracteres o si el email es invalido.',

    //Iniciar sesion
    'signin.form.title': 'Iniciar sesión',
    'signin.form.username': 'Usuario',
    'signin.form.password': 'Contraseña',
    'signin.form.rememberme': 'Iniciar sesión automáticamente',
    'signin.form.signup': 'Registrarse',
    'signin.form.forgotpassword': 'Ha olvidado su contraseña?',
    'signin.form.button.submit': 'Iniciar sesión',
    'signin.error.fieldisempty': 'El usuario ó la contraseña están vacios',
    'signin.error.accessdenied': 'El inicio de sesión ha fallado! Por favor intente de nuevo.',

    //Activity
    'activity.form.button.create': 'Crear actividad',
    'activity.form.button.update': 'Actualizar actividad',
    'activity.modal.name': 'Nombre',
    'activity.modal.description': 'Descripcion',
    'activity.modal.muscle': 'Musculo asociado',
    'activity.modal.duration': 'Duracion',
    'activity.modal.muscle.arms': 'Brazos',
    'activity.modal.muscle.legs': 'Piernas',
    'activity.modal.muscle.chest': 'Pecho',
    'activity.modal.muscle.shoulders': 'Hombros',
    'activity.modal.muscle.abs': 'Abs',
    'activity.modal.muscle.back': 'Espalda',
    'activity.modal.delete.confirmation': 'Esta seguro de eliminar esta actividad?',
    'activity.msg.creationsuccess': 'Actividad creada exitosamente!',
    'activity.msg.updatesuccess': 'Actividad actualizada exitosamente!',
    'activity.msg.emptyactivity.title': 'Hum Primera vez aqui',
    'activity.msg.emptyactivity.content': 'Por favor crea tu primera actividad',

    //Entrenamiento
    'workout.form.button.create': 'Crear entrenamiento',
    'workout.form.button.updateworkout': 'Actualizar workout',
    'workout.modal.name': 'Nombre',
    'workout.modal.description': 'Descripcion',
    'workout.modal.difficulty': 'Dificultad',
    'workout.modal.duration': 'Duracion',
    'workout.modal.activities': 'Actividades',
    'workout.modal.difficult.easy': 'Facil',
    'workout.modal.difficult.normal': 'Normal',
    'workout.modal.difficult.hard': 'Avanzada',
    'workout.modal.delete.confirmation': 'Esta seguro de eliminar el entrenamiento?',
    'workout.error.internalservererror': 'Error! Un problema ha occurrido al enviar la informacion',
    'workout.error.workoutexists': 'Error! Existe un entrenamiento con el mismo nombre. Por favor escoja otro nombre',
    'workout.msg.creationsuccess': 'Entrenamiento creado exitosamente!',
    'workout.msg.updatesuccess': 'Entrenamiento actualizado exitosamente!',
    'workout.msg.createactivity.title': 'Ups... Actividades primero',
    'workout.msg.createactivity.content': 'Para crear entrenamientos, primero necesitas crear actividades.',

    //My body
    'body.form.button.update': 'Actualizar mi cuerpo',
    'body.modal.weight': 'Peso',
    'body.modal.height': 'Altura',
    'body.modal.fat': 'Porcentaje de grasa',
    'body.modal.muscle': 'Porcentaje muscular',

    //Errors
    'error.internalservererror': 'Error! A problem has been occurred while submitting your data',
};