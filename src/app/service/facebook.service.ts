import {Injectable} from '@angular/core';
import { Router } from '@angular/router';
import {noUndefined} from "@angular/compiler/src/util";

declare var window: any;
declare var FB: any;

@Injectable()
export class Facebook {

    constructor(public router:Router) {
        if (!window.fbAsyncInit) {
            // console.log('constructor fb serv');
            window.fbAsyncInit = function() {
                FB.init({
                    appId      : '160848754449445',
                    cookie     : true,
                    xfbml      : true,
                    version    : 'v2.8'
                });
            };
        }
    }

    loadAndInitFBSDK(){
        var js,
            id = 'facebook-jssdk',
            ref = document.getElementsByTagName('script')[0];

        if (document.getElementById(id)) {
            return;
        }

        js = document.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/sdk.js";

        ref.parentNode.insertBefore(js, ref);
        console.log('loading fb service');

        // FB.login(function(response:any) {
        //     if (response.authResponse) {
        //         console.log('Welcome!  Fetching your information.... ');
        //         this.router.navigate(['home']);
        //     } else {
        //         console.log('User cancelled login with fb or did not fully authorize.');
        //     }
        // });
    }

    isLoggedWithFb(){
        // FB.getLoginStatus(function(response:any) {
        //     if (response.status === 'connected') {
        //         console.log('Logged in.');
        //         return true;
        //     }
        //     return false;
        // });
    }

}
