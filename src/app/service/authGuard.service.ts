import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import {JwtHelper} from 'angular2-jwt';

@Injectable()
export class AuthGuard implements CanActivate {
    private jwtHelper: JwtHelper = new JwtHelper();

    constructor(private router: Router) {}

    canActivate() {
        if (this.isAuthenticated()) {
            return true;
        }

        this.router.navigate(['/login']);
        return false;
    }

    isAuthenticated(){
        let token: string = localStorage.getItem('id_token');
        return token && !this.jwtHelper.isTokenExpired(token);
    }

    getToken(){
        return localStorage.getItem('id_token');
    }
}