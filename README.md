# WORKOUTWISE 

A simple base project for Angular 2 apps, using Gulp to automate tasks and SystemJS to bundle the app.


## Usage

Clone the repo to where you want to use it, then install the npm packages with `npm i`. 

To generate the output directory `dist/` run the command `gulp`.

If you add more `@angular` packages, you will need to update the `system.config.js` file to include them in the bundle during the build process.

## For Development

Run the command `npm run start-dev`. When it changes something in the code, it automatically reloads the system


## Production

For a production setup, uncomment the lines in `main.ts` to put Angular in production mode. Then, run the minification task to minify vendor files `gulp minify`.

## Auto-Refresh when Developing

You may run `gulp watch` in a stand-alone terminal to have the output automatically updated when you save changes.

###Login
For testing purposes, use the following credentials

Username: user
Password: user